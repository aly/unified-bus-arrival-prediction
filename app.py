import datetime
import hashlib
import os

import requests
import werkzeug.exceptions
from flask import Flask, render_template, request, abort, redirect, url_for

app = Flask(__name__)
app.jinja_env.lstrip_blocks = True
app.jinja_env.trim_blocks = True


BASE_URL = "https://api.tfl.gov.uk"
ATTRS = ["timeToStation", "vehicleId"]


def get_api_keys():
    return {
        "app_id": os.environ.get("UNIFIED_APP_ID"),
        "app_key": os.environ.get("UNIFIED_APP_KEY")
    }


def sha256(m):
    return hashlib.sha256(str.encode(str(m))).hexdigest()


# Do not cache any pages.
@app.after_request
def add_header(response):
    response.cache_control.max_age = 0
    response.cache_control.no_cache = True
    return response


@app.route("/")
def main():
    render_vars = {
        "release_version": os.environ.get("HEROKU_RELEASE_VERSION")
    }

    return render_template("home.html", **render_vars)


@app.route("/gps")
def gps():
    return render_template("gps.html")


@app.route("/search")
def search():
    keys = get_api_keys()
    params = dict()
    if keys["app_id"] and keys["app_key"]:
        params.update(keys)

    sms_code = request.args.get("sms_code")
    lat, lon = map(request.args.get, ["lat", "lon"])

    if sms_code:
        if not len(sms_code) == 5:
            abort(400, "Bus stop SMS code specified is not 5 digits long")

        url = f"{BASE_URL}/StopPoint/Search/{sms_code}"
        resp = requests.get(url, params=params)
        if not resp.ok:
            abort(502, resp)

        matches = resp.json()["matches"]
        if not len(matches) > 0:
            abort(400, "Invalid bus stop SMS code")

        stop_point_id = matches[0]["id"]

        return redirect(url_for("prediction", stop_id=stop_point_id), code=302)

    elif lat and lon:
        url = f"{BASE_URL}/StopPoint"
        params.update({
            "lat": lat,
            "lon": lon,
            "stopTypes": "NaptanPublicBusCoachTram",
            "radius": "300"
        })

        resp = requests.get(url, params=params)
        if not resp.ok:
            abort(502, resp)

        stop_points = resp.json()["stopPoints"]
        if not len(stop_points) > 0:
            # FIXME: This shouldn't be a 4xx error, but the 5xx's aren't
            # set up to deal with these things...
            abort(400, "Cannot find any bus stops near your location")

        return render_template("search_results.html", **{
            "result": stop_points
        })

    else:
        abort(400, "Invalid search criteria")


@app.route("/prediction/<stop_id>")
def prediction(stop_id):
    keys = get_api_keys()
    params = dict()
    if keys["app_id"] and keys["app_key"]:
        params.update(keys)

    resp = requests.get(f"{BASE_URL}/StopPoint/{stop_id}/Arrivals", params=params)

    if not resp.ok:
        abort(502, resp)

    # Dunno why this doesn't get sorted automatically
    resp = sorted(resp.json(), key=lambda bus: bus["timeToStation"])

    return render_template("prediction.html", **{
        "result": resp,
        "attrs": ATTRS,
        "created_utc": datetime.datetime.utcnow().strftime("%d %b %Y, %H:%M:%S UTC")
    })


@app.route("/keycheck")
def key_check():
    keys = get_api_keys()

    return render_template("key_check.html", **{
        "app_keys_set": (keys["app_id"] and keys["app_key"]),
        "sha256_app_id": sha256(keys["app_id"]),
        "sha256_app_key": sha256(keys["app_key"])
    })


@app.errorhandler(werkzeug.exceptions.BadRequest)
def handle_bad_request(e):
    return render_template("400.html", e=e), 400


@app.errorhandler(werkzeug.exceptions.BadGateway)
def handle_bad_gateway(e):
    return render_template("502.html", e=e), 500


if __name__ == '__main__':
    app.run()
